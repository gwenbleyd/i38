import requests, random, string, csv, os
import tkinter as tk
import time
from datetime import datetime, timedelta
from openpyxl import Workbook
import config

temp_folder = os.environ.get('TEMP')

date_viewing = 1

def csv_to_xlsx_beautify(csv_filename):
    from openpyxl.styles import Alignment, Border, Font, Side
    with open(csv_filename, encoding='utf-8-sig') as file:
        reader = csv.reader(file, delimiter=';')
        data = list(reader)
    workbook = Workbook()
    sheet = workbook.active
    for row in data:
        sheet.append(row)
    for row in sheet.iter_rows(min_row=2, min_col=1, max_col=1):
        for cell in row:
            cell.value = datetime.strptime(cell.value, "%Y-%m-%d %H:%M:%S") + timedelta(hours=3)
    border = Border(left=Side(border_style='thin', color='000000'),
                    right=Side(border_style='thin', color='000000'),
                    top=Side(border_style='thin', color='000000'),
                    bottom=Side(border_style='thin', color='000000'))
    for column in sheet.columns:
        column_letter = column[0].column_letter
        sheet.column_dimensions[column_letter].width = 23.6
    for row in sheet.rows:
        sheet.row_dimensions[row[0].row].height = 30
    font = Font(name='Tahoma', size=12)
    for row in sheet.iter_rows():
        for cell in row:
            cell.border = border
            cell.font = font
            cell.alignment = Alignment(vertical='center', horizontal='center', wrap_text=True)
    sheet.auto_filter.ref = "A1:" + sheet.cell(sheet.max_row, sheet.max_column).coordinate
    workbook.save(os.path.join(config.PATH_FINALLY, os.path.basename(csv_filename.replace('.csv', '.xlsx'))))


def generate_random_string(length):
    characters = string.ascii_letters + string.digits
    return ''.join(random.choice(characters) for i in range(length))


def generate_tuple_string(fer_sessions):
    fer_sessions = ["'" + session + "'" for session in fer_sessions]
    result = "(" + ", ".join(fer_sessions) + ")"
    return result


def parse_fer_conc():
    from selenium import webdriver
    from selenium.webdriver.common.by import By
    from selenium.webdriver.common.keys import Keys
    from selenium.webdriver.support.ui import Select
    from selenium.webdriver.support.ui import WebDriverWait
    from selenium.common.exceptions import TimeoutException
    from selenium.webdriver.support import expected_conditions as EC
    import re
    from selenium.webdriver.chrome.options import Options
    from selenium.webdriver.chrome.service import Service
    from selenium.webdriver.common.action_chains import ActionChains

    path = os.path.dirname(config.PATH_EXTENTION)

    options = Options()
    options.add_argument('--disable-blink-features=AutomationControlled')
    options.add_argument(f"--load-extension={path}")
    driver = webdriver.Chrome(options=options, service_args=['--ignore-ssl-errors=true'])
    driver.delete_all_cookies()

    driver.get('https://fer-concentrator.egisz.rosminzdrav.ru/concentrator_web/audit.htm')
    while True:
        try:
            WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.ID, "login")))
            break
        except TimeoutException:
            driver.refresh()

    WebDriverWait(driver, 15).until(EC.visibility_of_element_located(
        (By.XPATH, '/html/body/esia-root/div/esia-login/div/div[1]/form/div[4]/div/div[2]/div[2]/button')))
    driver.find_element(By.XPATH,
                        '/html/body/esia-root/div/esia-login/div/div[1]/form/div[4]/div/div[2]/div[2]/button').click()
    WebDriverWait(driver, 15).until(EC.visibility_of_element_located(
        (By.XPATH, '/html/body/esia-root/div/esia-login/div/div[1]/esia-eds/div/button')))
    driver.find_element(By.XPATH, '/html/body/esia-root/div/esia-login/div/div[1]/esia-eds/div/button').click()
    WebDriverWait(driver, 15).until(EC.visibility_of_element_located(
        (By.XPATH, '/html/body/esia-root/esia-modal/div/div[2]/div/ng-component/div/esia-eds-item[2]/div/button')))
    driver.find_element(By.XPATH,
                        '/html/body/esia-root/esia-modal/div/div[2]/div/ng-component/div/esia-eds-item[2]/div/button').click()
    WebDriverWait(driver, 15).until(EC.visibility_of_element_located((By.XPATH,
                                                                      '/html/body/esia-root/esia-modal/div/div[2]/div/ng-component/div/div[1]/div[1]/esia-input-password/div/input')))
    driver.find_element(By.XPATH,
                        '/html/body/esia-root/esia-modal/div/div[2]/div/ng-component/div/div[1]/div[1]/esia-input-password/div/input').send_keys(
        '123')
    driver.find_element(By.XPATH,
                        '/html/body/esia-root/esia-modal/div/div[2]/div/ng-component/div/div[1]/div[2]/button').click()

    time.sleep(10)

    while True:
        try:
            EC.url_contains("https://fer-concentrator.egisz.rosminzdrav.ru/concentrator_web/audit.htm")
            break
        except TimeoutException:
            driver.refresh()

    WebDriverWait(driver, 10).until(
        EC.url_contains("https://fer-concentrator.egisz.rosminzdrav.ru/concentrator_web/audit.htm")
    )

    yesterday_start = datetime.strftime(datetime.now() - timedelta(date_viewing), '%d%m%Y000001')
    yesterday_end = datetime.strftime(datetime.now() - timedelta(date_viewing), '%d%m%Y235959')
    driver.find_element(By.ID, 'dateTimeFrom').send_keys(Keys.BACKSPACE)
    driver.find_element(By.ID, 'dateTimeFrom').send_keys(yesterday_start)

    driver.find_element(By.ID, 'dateTimeTo').send_keys(Keys.BACKSPACE)
    driver.find_element(By.ID, 'dateTimeTo').send_keys(yesterday_end)

    Select(driver.find_element(By.ID, 'statusCode')).select_by_value('ERROR')
    Select(driver.find_element(By.ID, 'subServiceId')).select_by_value('1')

    driver.find_element(By.XPATH, '//*[@id="regionForm"]/div[1]/div/div/div/div[7]/div/div/button[1]').click()

    match = re.search(r"\d+", driver.find_element(By.XPATH, '//*[@id="regionForm"]/div[2]/div/ul/li[9]/a')
                      .get_attribute("onclick"))
    if match:
        number = match.group()
    else:
        print("No match found")

    table_data = []

    for i in range(1, int(number) + 1):
        current_page = driver.find_element(By.ID, 'currentPage')
        driver.execute_script("arguments[0].type = 'text';", current_page)
        current_page.clear()
        current_page.send_keys(i)
        current_page.submit()
        table = driver.find_element(By.ID, 'auditTable')
        rows = table.find_elements(By.TAG_NAME, 'tr')
        for row in rows:
            table_data.append([td.text for td in row.find_elements(By.TAG_NAME, 'td')])

    table_data = list(filter(None, table_data))
    result_list = []
    for elem in table_data:
        result_list.append(elem[2].split(' ')[2])
    driver.quit()
    return result_list


fer_sessions = generate_tuple_string(parse_fer_conc())

session = requests.Session()

headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/102.0.0.0 Safari/537.36'}

login_data = {
    'username': config.USERNAME,
    'password': config.PASSWORD
}

response = session.post(f'{config.URL}/login/', data=login_data, headers=headers)

yesterday = datetime.strftime(datetime.now() - timedelta(date_viewing), '%Y-%m-%d')
sql = f"""SELECT session_begin_ts AS `Время начала сессии`,
       fer_session_id AS `ID сессии (Федеральный)`,
       lpu_service_mis AS `Наименование МИС`,
       replace(lpu_level1_short_name, '\"', '') AS `Краткое наименование юр. лица МО`,
       replace(lpu_level2_short_name, '\"', '') AS `Краткое наименование подразделения МО`,
       hub_lpu_id AS `ID ЛПУ (Региональный)`,
       in_patient_id AS `ID Пациента`,
       fer_method_name AS `Название метода (Федеральный)`,
       fer_method_call_success AS `Результат вызова метода (Федеральный, 0 - ошибка 1 - успешно)`,
       hub_method_call_success AS `Результат вызова метода (Региональный, 0 - ошибка 1 - успешно)`,
       fer_error_description AS `Описание ошибки (Федеральный)`,
       REPLACE(REPLACE(hub_error_description, '\n', ''), '\"', '') AS `Описание ошибки (Региональный)`,
       multiIf(hub_method_call_success=1,'Успех регметода', hub_method_call_success=0,'Ошибка регметода', 'Другие') AS `Описание результата вызова метода (Региональный)`,
       fer_error_code AS `Код ошибки (Федеральный)`,
       fer_error_text AS `Текст ошибки (Федеральный)`,
       empty_response AS `Признак пустого ответа (0 - не пустой 1 - пустой)`,
       hub_method_name AS `Название метода (Региональный)`,
       hub_error_id AS `ID ошибки (Региональный)`,
       hub_error_text AS `Текст ошибки (Региональный)`,
       in_position_id AS `ID Должности`,
       position_name AS `Должность`,
       in_doctor_id AS `ID Врача`,
       doctor_name AS `Имя врача`,
       uniqExact(`ID сессии (Федеральный)`) AS `Число сессий пользователя с ЕПГУ`
FROM r47.zpv_fed_failed_sessions
WHERE fer_session_id in {fer_sessions}
GROUP BY `Время начала сессии`,
         `ID сессии (Федеральный)`,
         `Наименование МИС`,
         `Краткое наименование юр. лица МО`,
         `Краткое наименование подразделения МО`,
         `ID ЛПУ (Региональный)`,
         `ID Пациента`,
         `Название метода (Федеральный)`,
         `Результат вызова метода (Федеральный, 0 - ошибка 1 - успешно)`,
         `Результат вызова метода (Региональный, 0 - ошибка 1 - успешно)`,
         `Описание ошибки (Федеральный)`,
         `Описание ошибки (Региональный)`,
         `Описание результата вызова метода (Региональный)`,
         `Код ошибки (Федеральный)`,
         `Текст ошибки (Федеральный)`,
         `Признак пустого ответа (0 - не пустой 1 - пустой)`,
         `Название метода (Региональный)`,
         `ID ошибки (Региональный)`,
         `Текст ошибки (Региональный)`,
         `ID Должности`,
         `Должность`,
         `ID Врача`,
         `Имя врача`
ORDER BY `ID сессии (Федеральный)`, `Время начала сессии`
limit 65000 OFFSET 0
"""

client_id = generate_random_string(10)

data = {
    'client_id': client_id,
    'database_id': '2',
    'schema': 'r47',
    'sql': sql
}

report = f'{config.URL}/superset/sql_json/'

response = session.post(report, data=data)

response = session.get(f'{config.URL}/superset/csv/{client_id}')

save_path = os.path.join(temp_folder, 'Ответ от нетрики.csv')

with open(save_path, 'wb') as f:
    f.write(response.content)

doctor_list = ['врач-акушер-гинеколог',
               'врач - детский хирург',
               'врач общей практики (семейный врач)',
               'врач-оториноларинголог',
               'врач-офтальмолог',
               'врач-педиатр участковый',
               'врач-психиатр детский',
               'врач-психиатр-нарколог',
               'врач-психиатр подростковый',
               'врач-стоматолог',
               'врач-стоматолог детский',
               'врач-стоматолог-терапевт',
               'врач-терапевт участковый',
               'врач-фтизиатр',
               'врач-хирург'
               ]

with open(save_path, encoding='utf-8-sig') as f:
    reader = csv.DictReader(f, delimiter=';')
    session_groups = []
    current_group = None
    for row in reader:
        if not current_group or row['ID сессии (Федеральный)'] != current_group['id']:
            if current_group:
                session_groups.append(current_group)
            current_group = {
                'id': row['ID сессии (Федеральный)'],
                'sessions': [],
                'i38': 0
            }
        current_group['sessions'].append(row)
    if current_group:
        session_groups.append(current_group)

sorted(session_groups, key=lambda x: x['id'])
for elem in session_groups:
    elem['sessions'].sort(key=lambda x: x['Время начала сессии'])

for session in session_groups:
    for elem in session['sessions']:
        if not elem.get('Краткое наименование юр. лица МО'):
            for other_elem in session['sessions']:
                if other_elem.get('Краткое наименование юр. лица МО'):
                    elem['Краткое наименование юр. лица МО'] = other_elem['Краткое наименование юр. лица МО']
                    break

for i in session_groups:
    for j in i['sessions']:
        if j['ID ЛПУ (Региональный)'] != '':
            j['ID ЛПУ (Региональный)'] = j['ID ЛПУ (Региональный)'].replace('.0', '')
        if j['Результат вызова метода (Федеральный, 0 - ошибка 1 - успешно)'] != '' and '.0' in j[
            'Результат вызова метода (Федеральный, 0 - ошибка 1 - успешно)']:
            j['Результат вызова метода (Федеральный, 0 - ошибка 1 - успешно)'] = j[
                'Результат вызова метода (Федеральный, 0 - ошибка 1 - успешно)'].replace('.0', '')
        if j['Результат вызова метода (Региональный, 0 - ошибка 1 - успешно)'] != '' and '.0' in j[
            'Результат вызова метода (Региональный, 0 - ошибка 1 - успешно)']:
            j['Результат вызова метода (Региональный, 0 - ошибка 1 - успешно)'] = j[
                'Результат вызова метода (Региональный, 0 - ошибка 1 - успешно)'].replace('.0', '')

all_save_path = os.path.join(temp_folder, f'Тех. ошибки {yesterday}.csv')

with open(all_save_path, 'w', newline='', encoding='utf-8-sig') as file:
    fieldnames = ['Время начала сессии', 'ID сессии (Федеральный)', 'Наименование МИС',
                  'Краткое наименование юр. лица МО', 'Краткое наименование подразделения МО', 'ID ЛПУ (Региональный)',
                  'ID Пациента', 'Название метода (Федеральный)',
                  'Результат вызова метода (Федеральный, 0 - ошибка 1 - успешно)',
                  'Результат вызова метода (Региональный, 0 - ошибка 1 - успешно)', 'Описание ошибки (Федеральный)',
                  'Описание ошибки (Региональный)', 'Описание результата вызова метода (Региональный)',
                  'Код ошибки (Федеральный)', 'Текст ошибки (Федеральный)',
                  'Признак пустого ответа (0 - не пустой 1 - пустой)', 'Название метода (Региональный)',
                  'ID ошибки (Региональный)', 'Текст ошибки (Региональный)', 'ID Должности', 'Должность', 'ID Врача',
                  'Имя врача', 'Число сессий пользователя с ЕПГУ']
    writer = csv.DictWriter(file, fieldnames=fieldnames, delimiter=';')
    writer.writeheader()
    for i in session_groups:
        for j in i['sessions']:
            writer.writerow(j)
csv_to_xlsx_beautify(all_save_path)

os.remove(save_path)
os.remove(all_save_path)
